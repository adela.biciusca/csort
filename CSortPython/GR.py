#Importuri biblioteci
import cv2 #OpenCV pentru procesare imagine
import numpy as np
import time
import RPi.GPIO as GPIO  #Biblioteca pentru control GPIO Raspberry Pi


lower_red = np.array([0, 70, 50]) #Hue, Saturation, Value, limite superioare si inferioare pentru rosu
upper_red = np.array([10, 255, 255])  

lower_green = np.array([90, 50, 50])  #Hue, Saturation, Value, limite superioare si inferioare pentru verde
upper_green = np.array([145, 255, 255])  

#Capturare video (camera)
webcam_video = cv2.VideoCapture(0)

#Setare GPIO catre pinii fizici (servomotor)
GPIO.setmode(GPIO.BOARD)

#Setare pin 11 ca iesire, frecventa 50 Hz (servomotor)
GPIO.setup(11,GPIO.OUT)
servo1 = GPIO.PWM(11,50) 

#Pornire servomotor cu PWM 0 (servomotor oprit)
servo1.start(0)


is_green_detected = False
is_red_detected = False
last_detection_time = time.time() #Setare variablila cu timpul curent

while True:
    success, video = webcam_video.read()  #Citire video
    img = cv2.cvtColor(video, cv2.COLOR_BGR2HSV)  #Conversie din BGR in HSV

    mask_red = cv2.inRange(img, lower_red, upper_red)   #Mascare imagine pentru a obtine masca pentru rosu
    mask_green = cv2.inRange(img, lower_green, upper_green)   #Mascare imagine pentru a obtine masca pentru verde

    #Detectare obiecte rosii
    if cv2.countNonZero(mask_red) > 0 and not is_red_detected and not is_green_detected:
        is_red_detected = True
        last_detection_time = time.time() #Salvare timp curent
        servo1.ChangeDutyCycle(7.5)  #Rotire servomotor la stanga
        time.sleep(2)
        servo1.ChangeDutyCycle(5)
        time.sleep(0.5)
        servo1.ChangeDutyCycle(0)
        
    #Detectare obiecte verzi
    if cv2.countNonZero(mask_green) > 0 and not is_green_detected and not is_red_detected:
        is_green_detected = True
        last_detection_time = time.time()  #Salvare timp curent
        servo1.ChangeDutyCycle(2.5) #Rotire servomotor la dreapta
        time.sleep(2)
        servo1.ChangeDutyCycle(5)
        time.sleep(0.5)
        servo1.ChangeDutyCycle(0)
        
        

    #Verificare daca au trecut 3 secunde de la ultima detectie de culoare
    if time.time() - last_detection_time > 3:
        is_green_detected = False
        is_red_detected = False

    key = cv2.waitKey(1)  
    if key == 27:  
        break

#Release captura video
webcam_video.release()

#Inchidere ferestre OpenCv
cv2.destroyAllWindows()


