package com.example.csort;

import androidx.appcompat.app.AppCompatActivity; // Import clasa AppCompatActivity din biblioteca AndroidX
import android.os.AsyncTask; // Import clasa pentru operatiile de fundal
import android.os.Bundle; // Import clasa pentru a trimite informatii intre activitati
import android.view.View; // Import clasa pentru componentele UI
import android.widget.Button; // Import clasa pentru butoane
import com.jcraft.jsch.ChannelExec; // Import clasa ChannelExec din biblioteca JSch pentru executare comenzi la distanta
import com.jcraft.jsch.JSch; // Import clasa JSch din biblioteca JSch pentru conexiuni SSH
import com.jcraft.jsch.JSchException; // Import clasa JSchException din biblioteca JSch pentru pentru exceptiile legate de JSch
import com.jcraft.jsch.Session; // Import clasa Session din biblioteca JSch pentru stabilire si lucrare cu conexiunile SSH

public class MainActivity extends AppCompatActivity {

    // Declarare si initializare variabile pentru stabilire conexiune SSH si comenzi
    private String raspberryPiIp = "192.168.124.2";
    private String sshUsername = "licenta";
    private String sshPassword = "parola";
    private String commandToStartAppGR = "python GR.py";
    private String commandToStartAppBY = "python BY.py";
    private String commandToStopAppGR = "pkill -f GR.py";
    private String commandToStopAppBY = "pkill -f BY.py";
    private String commandToShutdown = "sudo /sbin/shutdown now";

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        // Butoanele din layout si variabilele corespunzatoare
        Button startAppGRButton = findViewById(R.id.start_application_gr_button);
        Button startAppBYButton = findViewById(R.id.start_application_by_button);
        Button stopAppGRButton = findViewById(R.id.stop_application_gr_button);
        Button stopAppBYButton = findViewById(R.id.stop_application_by_button);
        Button shutdownButton = findViewById(R.id.shutdown_button);

        // Setare listeners pentru butoane pentru a se executa o comanda cand se apasa click
        startAppGRButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                executeCommand(commandToStartAppGR);
            }
        });

        startAppBYButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                executeCommand(commandToStartAppBY);
            }
        });

        stopAppGRButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                executeCommand(commandToStopAppGR);
            }
        });

        stopAppBYButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                executeCommand(commandToStopAppBY);
            }
        });

        shutdownButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                executeCommand(commandToShutdown);
            }
        });
    }

    // Metoda care executa comanda SSH la distanta
    private void executeCommand(final String command) {
        new AsyncTask<Void, Void, Void>() {
            @Override
            protected Void doInBackground(Void... params) {
                try {
                    JSch jsch = new JSch();
                    //Creare o noua sesiune folosind SSH
                    Session session = jsch.getSession(sshUsername, raspberryPiIp, 22);
                    session.setPassword(sshPassword);
                    session.setConfig("StrictHostKeyChecking", "no");
                    // Conectare efectiva la sesiunea SSH
                    session.connect();

                    ChannelExec channel = (ChannelExec) session.openChannel("exec");
                    channel.setCommand(command);
                    channel.connect();
                    channel.disconnect();

                    session.disconnect();
                } catch (JSchException e) {
                    e.printStackTrace();
                }
                return null;
            }
        }.execute();
    }

    // Override metoda onResume pentru a se actualiza valorile
    @Override
    protected void onResume() {
        super.onResume();
    }
}
