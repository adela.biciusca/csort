# CSort

## Adresa repository-ului: [CSort](https://gitlab.upt.ro/adela.biciusca/csort)

## Configurare Raspberry Pi

- Asigurați-vă că pe aveți un card SD pe care este instalat Raspberry Pi OS
- Asigurați-vă că pe cardul SD aveți fișierele din directorul `CSortPython`

## Comunicare Raspberry Pi - aplicație Android

- Pentru a comunica cu aplicația mobilă asigurați-vă ca SSH-ul este activat în `Raspberry Pi Configuration`
- În fișierul `MainActivity.java` modificați variabilele `sshUsername` și `sshUsername` pentru a corespunde cu setările Raspberry Pi
- În fișierul `MainActivity.java` modificați variabila `raspberryPiIp` cu
  IP-ul Raspberry Pi cu ajutorul comenzii

```shell
hostname -I
```

- Asigurați-vă că atât dispozitivul pe care se rulează aplicația mobilă cât și Raspberry Pi sunt conectate la aceeași rețea

## Configurare aplicație Android

- Instalați Android Studio
- Instalați Java Development Kit
- Clonați repository-ul `CSort` cu ajutorul comenzii

```shell
git clone https://gitlab.upt.ro/adela.biciusca/csort
```

- Deschideți proiectul cu ajutorul Android Studio
- Asigurați-vă că aveți un emulator instalat sau folosiți propriul telefon mobil
- Apăsați pe butonul `Run`
